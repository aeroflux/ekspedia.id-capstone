package ekspedia.id.ekspedia.auth;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ekspedia.id.ekspedia.MainActivity;
import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.apihelper.BaseApiService;
import ekspedia.id.ekspedia.apihelper.UtilsApi;
import ekspedia.id.ekspedia.auth.RegisterActivity;
import ekspedia.id.ekspedia.model.Pendaki;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    TextView linkForgot, linkRegister;
    EditText inputEmail, inputPassword;
    Button btnLogin;
    ProgressDialog loading;

    Context context;
    BaseApiService apiService;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;
        apiService = UtilsApi.getApiService();
        initComponents();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loading = ProgressDialog.show(context, null, "Harap Tunggu...", true, false);
                requestLogin();
            }
        });

        linkForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        linkRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initComponents() {
        linkForgot = findViewById(R.id.linkForgot);
        linkRegister = findViewById(R.id.linkRegister);
        inputEmail = findViewById(R.id.loginEmail);
        inputPassword = findViewById(R.id.loginPassword);
        btnLogin = findViewById(R.id.buttonLogin);
    }

    public void toSignUp(View view) {
        Intent intent = new Intent(context, RegisterActivity.class);
        startActivity(intent);
    }

    private void requestLogin() {
        String user = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();

        apiService.login(new Pendaki(user, password))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        // ketika respon sukses
                        if (response.isSuccessful()) {
                            try {
                                JSONObject result = new JSONObject(response.body().string());
                                // ketika login berhasil
                                if (result.getString("error").equals("false")) {
                                    // mendapatkan informasi username dari JSON Object
                                    String token = result.getJSONObject("data").getString("token");
                                    Hawk.put("token", token);

                                    // intent ke main activity
                                    Intent intent = new Intent(context, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    String err_msg = result.getString("message");
                                    Toast.makeText(context, err_msg, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException jsonException) {
                                jsonException.printStackTrace();
                            } catch (IOException ioException) {
                                ioException.printStackTrace();
                            } finally {
                                loading.dismiss(); // dismiss loading
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.d("debug", "onFailure: ERROR > " + t.toString());
                    }
                });
    }
}
