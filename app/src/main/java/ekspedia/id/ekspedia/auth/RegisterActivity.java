package ekspedia.id.ekspedia.auth;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ekspedia.id.ekspedia.MainActivity;
import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.apihelper.BaseApiService;
import ekspedia.id.ekspedia.apihelper.UtilsApi;
import ekspedia.id.ekspedia.model.Pendaki;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    EditText inputUsername, inputEmail, inputPassword, inputPhone, inputName;
    TextView linkLogin;
    Button btnRegister;
    ProgressDialog loading;

    Context context;
    BaseApiService apiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        context = this;
        apiService = UtilsApi.getApiService();

        initComponents();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loading = ProgressDialog.show(context, null, "Harap Tunggu...",
                        true, false);
                requestRegister();
            }
        });

        linkLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initComponents() {
        inputUsername = findViewById(R.id.registerUsername);
        inputName = findViewById(R.id.registerName);
        inputPhone = findViewById(R.id.registerPhone);
        inputEmail = findViewById(R.id.registerEmail);
        inputPassword = findViewById(R.id.registerPassword);
        btnRegister = findViewById(R.id.buttonRegister);
        linkLogin = findViewById(R.id.linkLogin);
    }

    private void requestRegister() {
        String fullname = inputName.getText().toString();
        final String username = inputUsername.getText().toString();
        String phone = inputPhone.getText().toString();
        String email = inputEmail.getText().toString();
        String password = inputPassword.getText().toString();

        apiService.register(new Pendaki(fullname, phone, username, email, password))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject result = new JSONObject(response.body().string());
                                if (result.getBoolean("error") == false) {
                                    Hawk.put("username", username);
                                    Hawk.put("token", result.getJSONObject("data").getString("token"));

                                    Intent intent = new Intent(context, MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(context, "REGISTER GAGAL", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                    }
                });
    }

    private void testingIntent() {
        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
