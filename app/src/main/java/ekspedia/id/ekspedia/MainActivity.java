package ekspedia.id.ekspedia;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.orhanobut.hawk.Hawk;

import ekspedia.id.ekspedia.auth.LoginActivity;
import ekspedia.id.ekspedia.fragments.ExploreFragment;
import ekspedia.id.ekspedia.fragments.HomeFragment;
import ekspedia.id.ekspedia.fragments.PorterFragment;
import ekspedia.id.ekspedia.fragments.PreparationFragment;
import ekspedia.id.ekspedia.fragments.EquipmentFragment;
import ekspedia.id.ekspedia.profile.ProfileActivity;

public class MainActivity extends AppCompatActivity {

    private ActionBar actionBar;

    BottomNavigationView navigation;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        Hawk.init(context).build();

        actionBar = getSupportActionBar();
        actionBar.setTitle("Home");

        navigation = findViewById(R.id.bottomNav);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment;
                switch (item.getItemId()) {
                    case R.id.navHome:
                        actionBar.setTitle("Home");
                        fragment = new HomeFragment();
                        loadFragment(fragment);
                        return true;
                    case R.id.navPorter:
                        actionBar.setTitle("Porter");
                        fragment = new PorterFragment();
                        loadFragment(fragment);
                        return true;
                    case R.id.navExplorer:
                        actionBar.setTitle("Explorer");
                        fragment = new ExploreFragment();
                        loadFragment(fragment);
                        return true;
                    case R.id.navRent:
                        actionBar.setTitle("Rent");
                        fragment = new EquipmentFragment();
                        loadFragment(fragment);
                        return true;
                    case R.id.navPreparation:
                        actionBar.setTitle("Preparation");
                        fragment = new PreparationFragment();
                        loadFragment(fragment);
                        return true;
                }
                return false;
            }
        });
        loadFragment(new HomeFragment());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (Hawk.get("token") != null){
            inflater.inflate(R.menu.opt_logged, menu);
        } else {
            inflater.inflate(R.menu.opt_guest, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.linkProfile:
                Intent profile = new Intent(context, ProfileActivity.class);
                startActivity(profile);
                return true;
            case R.id.linkAuth:
                Intent login = new Intent(context, LoginActivity.class);
                startActivity(login);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment);
        transaction.commit();
    }
}
