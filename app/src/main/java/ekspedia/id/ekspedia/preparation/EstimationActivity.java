package ekspedia.id.ekspedia.preparation;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import java.util.ArrayList;

import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.adapter.ListEstimationAdapter;
import ekspedia.id.ekspedia.apihelper.BaseApiService;
import ekspedia.id.ekspedia.apihelper.UtilsApi;
import ekspedia.id.ekspedia.model.Barang;

public class EstimationActivity extends AppCompatActivity {

    RecyclerView listPerlengkapan;

    ArrayList<Barang> barangs;
    ListEstimationAdapter listEstimationAdapter;
    TextView estimationLokasi;

    Context context;
    BaseApiService baseApiService;
    long gunungID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_estimation);

        context = this;
        baseApiService = UtilsApi.getApiService();
        barangs = new ArrayList<>();

        initComponent();

        if (getIntent() != null) {
            gunungID = getIntent().getLongExtra("id",0);
            estimationLokasi.setText(getIntent().getStringExtra("gunung"));
        }

        loadEstimation(gunungID);
        listEstimationAdapter = new ListEstimationAdapter(context, barangs);
        listEstimationAdapter.notifyDataSetChanged();

        listPerlengkapan.setAdapter(listEstimationAdapter);
        listPerlengkapan.setLayoutManager(new GridLayoutManager(context, 2));
    }

    private void loadEstimation(long gunungID) {
        switch ((int) gunungID) {
            case 1:
                barangs.add(new Barang("Head Lamp"));
                barangs.add(new Barang("Sleeping Bag"));
                barangs.add(new Barang("Carrier"));
                barangs.add(new Barang("Sepatu Gunung"));
                break;
            case 2:
                barangs.add(new Barang("Tent"));
                barangs.add(new Barang("Carrier"));
                barangs.add(new Barang("Dry Bag"));
                barangs.add(new Barang("Sepatu Gunung"));
                break;
            case 3:
                barangs.add(new Barang("Tent"));
                barangs.add(new Barang("Carrier"));
                barangs.add(new Barang("Head Lamp"));
                barangs.add(new Barang("Trekking Pole"));
                barangs.add(new Barang("Sepatu Hiking"));
                break;
            case 4:
                barangs.add(new Barang("Tent"));
                barangs.add(new Barang("Regular Bag"));
                barangs.add(new Barang("Dry Bag"));
                barangs.add(new Barang("Sepatu Hiking"));
                break;
            case 5:
                barangs.add(new Barang("Tent"));
                barangs.add(new Barang("Dry Bag"));
                barangs.add(new Barang("Sepatu Hiking"));
                break;
        }

    }

    private void initComponent() {
        estimationLokasi = findViewById(R.id.estimationLokasi);
        listPerlengkapan = findViewById(R.id.estimationListBarang);
    }
}
