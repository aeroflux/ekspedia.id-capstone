package ekspedia.id.ekspedia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import ekspedia.id.ekspedia.equipment.DetailEquipmentActivity;
import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.model.Barang;

public class ListEquipmentAdapter extends RecyclerView.Adapter<ListEquipmentAdapter.RecyclerViewHolderEquipment> {

    Context context;
    LayoutInflater inflater;
    List<Barang> barangList;


    public ListEquipmentAdapter(Context context, List<Barang> barangList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.barangList = barangList;
    }

    @Override
    public ListEquipmentAdapter.RecyclerViewHolderEquipment onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_category_equipment, parent, false);
        RecyclerViewHolderEquipment viewHolderEquipment = new RecyclerViewHolderEquipment(v);
        return viewHolderEquipment;
    }

    @Override
    public void onBindViewHolder(ListEquipmentAdapter.RecyclerViewHolderEquipment holder, int position) {
        Barang barang = barangList.get(position);

        holder.tvEquipment.setText(barang.getNama());
        Glide.with(context).load(barang.getImage()).into(holder.ivEquipment);
    }

    @Override
    public int getItemCount() {
        return barangList.size();
    }

    public void clear() {
        barangList.clear();
    }

    public class RecyclerViewHolderEquipment extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvEquipment;
        ImageView ivEquipment;

        public RecyclerViewHolderEquipment(View itemView) {
            super(itemView);

            tvEquipment = itemView.findViewById(R.id.barangName);
            ivEquipment = itemView.findViewById(R.id.barangImage);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int pos = getLayoutPosition();
            Barang barang = barangList.get(pos);

            Log.d("list barang", "onClick: " + barang.getId());
            Log.d("list barang", "onClick: " + barang.getKategori_id());

            Intent i = new Intent(context, DetailEquipmentActivity.class);
            i.putExtra("id", barang.getId());
            i.putExtra("kategori_id", barang.getKategori_id());
            i.putExtra("nama", barang.getNama());
            i.putExtra("deskripsi", barang.getDeskripsi());
            i.putExtra("foto", barang.getImage());
            i.putExtra("harga", barang.getHarga());
            i.putExtra("rating", barang.getRating());
            context.startActivity(i);
        }
    }
}
