package ekspedia.id.ekspedia.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.model.Barang;

public class ListEstimationAdapter extends RecyclerView.Adapter<ListEstimationAdapter.ListEstimationViewHolder> {

    Context context;
    ArrayList<Barang> barangs;
    LayoutInflater inflater;

    public ListEstimationAdapter(Context context, ArrayList<Barang> barangs) {
        this.context = context;
        this.barangs = barangs;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ListEstimationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_item_estimation, parent, false);

        return new ListEstimationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListEstimationAdapter.ListEstimationViewHolder holder, int position) {
        Barang barang = barangs.get(position);

        holder.nama.setText(barang.getNama());
//        Glide.with(context).load(barang.getImage()).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return barangs.size();
    }

    public class ListEstimationViewHolder extends RecyclerView.ViewHolder {
        TextView nama;
        ImageView image;

        public ListEstimationViewHolder(View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.estimationName);
            image = itemView.findViewById(R.id.estimationImage);
        }
    }
}
