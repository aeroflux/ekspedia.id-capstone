package ekspedia.id.ekspedia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

//import com.bumptech.glide.Glide;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.explore.DetailExploreActivity;
import ekspedia.id.ekspedia.model.Gunung;

/**
 * Created by hakimrizki on 11/03/18.
 */

public class ListExploreAdapter extends RecyclerView.Adapter<ListExploreAdapter.RecyclerViewHolderExplore> {

    Context context;
    LayoutInflater inflater;
    ArrayList<Gunung> gunungs;


    public ListExploreAdapter(Context context, ArrayList<Gunung> gunungs) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.gunungs = gunungs;
    }

    @Override
    public RecyclerViewHolderExplore onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_explore, parent, false);

        return new RecyclerViewHolderExplore(v);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolderExplore holder, int position) {
        Gunung gunung = gunungs.get(position);

        holder.exploreName.setText(gunung.getNama());
        holder.exploreDeskripsi.setText(gunung.getDeskripsi());
        holder.exploreKetinggian.setText(String.valueOf(gunung.getKetinggian()) + " mdpl");
        holder.exploreRating.setRating(gunung.getRating());

        Glide.with(context).load(gunung.getPhoto()).into(holder.explorePhoto);

    }

    @Override
    public int getItemCount() {
        return gunungs.size();
    }

    public void clear() {
        gunungs.clear();
    }

    public class RecyclerViewHolderExplore extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView exploreName, exploreDeskripsi, exploreKetinggian;
        RatingBar exploreRating;
        ImageView explorePhoto;

        public RecyclerViewHolderExplore(View itemView) {
            super(itemView);

            explorePhoto = itemView.findViewById(R.id.listGunungPhoto);
            exploreRating = itemView.findViewById(R.id.listGunungReview);
            exploreName = itemView.findViewById(R.id.listGunungName);
            exploreDeskripsi = itemView.findViewById(R.id.listGunungDeskripsi);
            exploreKetinggian = itemView.findViewById(R.id.listGunungTinggi);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getLayoutPosition();
            Gunung gunung = gunungs.get(position);

            Intent i = new Intent(context, DetailExploreActivity.class);
            i.putExtra("id", gunung.getGunung_id());
            context.startActivity(i);
        }
    }
}
