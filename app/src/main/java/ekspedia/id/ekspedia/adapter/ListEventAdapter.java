package ekspedia.id.ekspedia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.event.DetailEventActivity;
import ekspedia.id.ekspedia.model.Event;

public class ListEventAdapter extends RecyclerView.Adapter<ListEventAdapter.ListEventViewHolder> {

    Context context;
    ArrayList<Event> events;
    LayoutInflater inflater;

    public ListEventAdapter(Context context, ArrayList<Event> events) {
        this.context = context;
        this.events = events;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ListEventViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_item_event, parent, false);
        return new ListEventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListEventAdapter.ListEventViewHolder holder, int position) {
        Event event = events.get(position);

        holder.nama.setText(event.getNama());
        holder.tanggal.setText(event.getTanggal());
    }

    @Override
    public int getItemCount() {
        return events.size();
    }

    public class ListEventViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView nama, tanggal;

        public ListEventViewHolder(View itemView) {
            super(itemView);
            nama = itemView.findViewById(R.id.listEventNama);
            tanggal = itemView.findViewById(R.id.listEventTanggal);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Event event = events.get(getAdapterPosition());

            Intent intent = new Intent(context, DetailEventActivity.class);
            intent.putExtra("id", event.getId());
            intent.putExtra("nama", event.getNama());
            intent.putExtra("deskripsi", event.getDeskripsi());
            intent.putExtra("tanggal", event.getTanggal());
            context.startActivity(intent);
        }
    }
}
