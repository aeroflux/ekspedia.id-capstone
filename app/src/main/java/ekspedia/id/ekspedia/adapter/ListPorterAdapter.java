package ekspedia.id.ekspedia.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.model.Porter;
import ekspedia.id.ekspedia.porter.DetailPorterActivity;

public class ListPorterAdapter extends RecyclerView.Adapter<ListPorterAdapter.PorterViewHolder> {

    Context context;
    ArrayList<Porter> porters;
    LayoutInflater inflater;

    public ListPorterAdapter(Context context, ArrayList<Porter> porters) {
        this.context = context;
        this.porters = porters;
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public PorterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_item_porter, parent, false);

        return new PorterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListPorterAdapter.PorterViewHolder holder, int position) {
        Porter porter = porters.get(position);

        holder.nama.setText(porter.getNama());
        holder.lokasi.setText(porter.getLokasi());
        holder.rating.setRating(porter.getRating());
        Glide.with(context).load(porter.getImage()).into(holder.photo);
    }

    @Override
    public int getItemCount() {
        return porters.size();
    }

    public class PorterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView nama, lokasi;
        ImageView photo;
        RatingBar rating;

        public PorterViewHolder(View itemView) {
            super(itemView);

            nama = itemView.findViewById(R.id.listPorterNama);
            lokasi = itemView.findViewById(R.id.listPorterLokasi);
            photo = itemView.findViewById(R.id.listPorterPhoto);
            rating = itemView.findViewById(R.id.listPorterRating);

            itemView.setOnClickListener(this);
        }


        @Override
        public void onClick(View view) {
            Porter porter = porters.get(getAdapterPosition());

            Intent intent = new Intent(context, DetailPorterActivity.class);
            intent.putExtra("nama", porter.getNama());
            intent.putExtra("lokasi", porter.getLokasi());
            intent.putExtra("deskripsi", porter.getDeskripsi());
            intent.putExtra("image", porter.getImage());
            intent.putExtra("rating", porter.getRating());
            context.startActivity(intent);
        }
    }
}
