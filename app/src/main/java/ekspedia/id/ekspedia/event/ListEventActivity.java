package ekspedia.id.ekspedia.event;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.adapter.ListEventAdapter;
import ekspedia.id.ekspedia.apihelper.BaseApiService;
import ekspedia.id.ekspedia.apihelper.UtilsApi;
import ekspedia.id.ekspedia.model.Event;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListEventActivity extends AppCompatActivity {

    RecyclerView listEvent;

    ArrayList<Event> events;
    ListEventAdapter listEventAdapter;

    Context context;
    BaseApiService baseApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_event);

        context = this;
        Hawk.init(context).build();
        baseApiService = UtilsApi.getApiService();
        events = new ArrayList<>();

        initComponents();

        if (Hawk.get("token") != null)
            loadEvents();
        listEventAdapter = new ListEventAdapter(context, events);
        listEvent.setAdapter(listEventAdapter);
        listEvent.setLayoutManager(new LinearLayoutManager(context));
    }

    private void loadEvents() {
        baseApiService.getEvent(Hawk.get("token").toString())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject result = new JSONObject(response.body().string());
                                if (result.getBoolean("error") == false) {
                                    JSONArray array = result.getJSONObject("data").getJSONArray("event");
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject object = array.getJSONObject(i);
                                        Event event = new Event(
                                                object.getInt("id"),
                                                object.getString("nama"),
                                                object.getString("deskripsi"),
                                                object.getString("tanggal")
                                        );
                                        events.add(event);
                                    }
                                    listEventAdapter.notifyDataSetChanged();
                                } else {
                                    Toast.makeText(context, "gagal load event :(", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
    }

    private void initComponents() {
        listEvent = findViewById(R.id.listEventRecycler);
    }
}
