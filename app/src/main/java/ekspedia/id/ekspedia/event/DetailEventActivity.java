package ekspedia.id.ekspedia.event;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.apihelper.BaseApiService;
import ekspedia.id.ekspedia.apihelper.UtilsApi;

public class DetailEventActivity extends AppCompatActivity {

    ImageView image;
    TextView nama, deskripsi, tanggal;

    int eventID;

    Context context;
    BaseApiService baseApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_event);

        context = this;
        baseApiService = UtilsApi.getApiService();

        initComponents();

        if (getIntent() != null) {
            eventID = getIntent().getIntExtra("id", 0);
            nama.setText(getIntent().getStringExtra("nama"));
            deskripsi.setText(getIntent().getStringExtra("deskripsi"));
            tanggal.setText(getIntent().getStringExtra("tanggal"));
        }
    }

    private void initComponents() {
        nama = findViewById(R.id.detailEventNama);
        deskripsi = findViewById(R.id.detailEventDeskripsi);
        tanggal = findViewById(R.id.detailEventTanggal);
        image = findViewById(R.id.detailEventImage);
    }
}
