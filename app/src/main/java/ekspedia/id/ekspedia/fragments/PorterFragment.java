package ekspedia.id.ekspedia.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.adapter.ListPorterAdapter;
import ekspedia.id.ekspedia.apihelper.BaseApiService;
import ekspedia.id.ekspedia.apihelper.UtilsApi;
import ekspedia.id.ekspedia.model.Porter;

/**
 * A simple {@link Fragment} subclass.
 */
public class PorterFragment extends Fragment {

    RecyclerView listPorter;

    ArrayList<Porter> porters;
    ListPorterAdapter listPorterAdapter;

    Context context;
    BaseApiService baseApiService;

    public PorterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_porter, container, false);

        context = rootView.getContext();
        baseApiService = UtilsApi.getApiService();

        porters = new ArrayList<>();

        initComponents(rootView);

        loadPorter();
        listPorterAdapter = new ListPorterAdapter(context, porters);
        listPorter.setAdapter(listPorterAdapter);
        listPorter.setLayoutManager(new LinearLayoutManager(context));
        return rootView;
    }

    private void loadPorter() {
        porters.add(new Porter("Yuda", "Bandung", "Deskripsi porter Yuda",
                3, R.drawable.ic_user_silhouette));
        porters.add(new Porter("Nugraha", "Bandung", "Deskripsi porter Nugraha",
                2, R.drawable.ic_user_silhouette));
        porters.add(new Porter("Milla", "Bandung", "Deskripsi porter Milla",
                5, R.drawable.ic_user_silhouette));
        porters.add(new Porter("Fahri", "Bandung", "Deskripsi porter Fahri",
                4, R.drawable.ic_user_silhouette));
    }

    private void initComponents(View rootView) {
        listPorter = rootView.findViewById(R.id.listPorterRecycler);
    }

}
