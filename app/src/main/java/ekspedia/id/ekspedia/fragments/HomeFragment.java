package ekspedia.id.ekspedia.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.adapter.PopularAdapter;
import ekspedia.id.ekspedia.apihelper.BaseApiService;
import ekspedia.id.ekspedia.event.ListEventActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {

    GridView popular;
    TextView eventText;

    Context context;
    BaseApiService baseApiService;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        context = rootView.getContext();
        initComponents(rootView);
        eventText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context, ListEventActivity.class));
            }
        });

        popular.setAdapter(new PopularAdapter(rootView.getContext()));

        // Inflate the layout for this fragment
        return rootView;
    }

    private void initComponents(View rootView) {
        popular = rootView.findViewById(R.id.listPopular);
        eventText = rootView.findViewById(R.id.eventText);
    }
}