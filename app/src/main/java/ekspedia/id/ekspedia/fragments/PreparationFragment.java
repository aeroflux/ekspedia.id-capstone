package ekspedia.id.ekspedia.fragments;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.preparation.EstimationActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreparationFragment extends Fragment {

    Spinner pilihGunung;
    EditText inputEstimasi, inputPendaki;
    Button buttonSubmit;

    Context context;

    public PreparationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_preparation, container, false);

        context = view.getContext();

        initComponents(view);
        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Log.v(context.getClass().getSimpleName(), String.valueOf(pilihGunung.getSelectedItemId()));

                Intent intent = new Intent(context, EstimationActivity.class);
                intent.putExtra("id", pilihGunung.getSelectedItemId());
                intent.putExtra("gunung", pilihGunung.getSelectedItem().toString());

                startActivity(intent);
            }
        });

        return view;
    }

    private void initComponents(View view) {
        pilihGunung = view.findViewById(R.id.preparationPilihGunung);
        inputEstimasi = view.findViewById(R.id.preparationEstimasi);
        inputPendaki = view.findViewById(R.id.preparationPendaki);
        buttonSubmit = view.findViewById(R.id.preparationButton);
    }

}
