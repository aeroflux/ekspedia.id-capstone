package ekspedia.id.ekspedia.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.adapter.ListEquipmentAdapter;
import ekspedia.id.ekspedia.apihelper.BaseApiService;
import ekspedia.id.ekspedia.apihelper.UtilsApi;
import ekspedia.id.ekspedia.model.Barang;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class EquipmentFragment extends Fragment {

    RecyclerView listOutdoor;
    RecyclerView listBackpack;
    RecyclerView listFootwear;

    ArrayList<Barang> outdoors;
    ArrayList<Barang> backpacks;
    ArrayList<Barang> footwears;
    ListEquipmentAdapter outdoorAdapter;
    ListEquipmentAdapter backpackAdapter;
    ListEquipmentAdapter footwearAdapter;

    Context context;
    BaseApiService baseApiService;

    public EquipmentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_equipment, container, false);

        context = view.getContext();
        Hawk.init(context).build();
        baseApiService = UtilsApi.getApiService();
        outdoors = new ArrayList<>();
        backpacks = new ArrayList<>();
        footwears = new ArrayList<>();

        initComponents(view);

        if (Hawk.get("token") != null)
            loadEquipmentCategories();

        outdoorAdapter = new ListEquipmentAdapter(context, outdoors);
        backpackAdapter = new ListEquipmentAdapter(context, backpacks);
        footwearAdapter = new ListEquipmentAdapter(context, footwears);

        listOutdoor.setAdapter(outdoorAdapter);
        listOutdoor.setLayoutManager(
                new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        );
        listBackpack.setAdapter(backpackAdapter);
        listBackpack.setLayoutManager(
                new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        );
        listFootwear.setAdapter(footwearAdapter);
        listFootwear.setLayoutManager(
                new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        );
        return view;
    }

    private void loadEquipmentCategories() {
        baseApiService.getAllBarang(Hawk.get("token").toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject result = new JSONObject(response.body().string());
                        if (result.getBoolean("error") == false) {
                            JSONArray array = result.getJSONObject("data").getJSONArray("kategori");
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.getJSONObject(i);

                                JSONArray array2 = object.getJSONArray("barang");
                                for (int j = 0; j < array2.length(); j++) {
                                    JSONObject object2 = array2.getJSONObject(j);
                                    Barang barang = new Barang(
                                            object2.getInt("id"),
                                            object2.getInt("kategori_id"),
                                            object2.getInt("bintang_review"),
                                            object2.getInt("harga"),
                                            object2.getString("nama"),
                                            object2.getString("deskripsi"),
                                            object2.getJSONObject("file").getString("path").replace("\\", "/")
                                    );
                                    if (object2.getInt("kategori_id") == 1) {
                                        outdoors.add(barang);
                                    }
                                    if (object2.getInt("kategori_id") == 2) {
                                        backpacks.add(barang);
                                    }
                                    if (object2.getInt("kategori_id") == 3) {
                                        footwears.add(barang);
                                    }
                                }
                                outdoorAdapter.notifyDataSetChanged();
                                backpackAdapter.notifyDataSetChanged();
                                footwearAdapter.notifyDataSetChanged();
                            }
                        } else {
                            Toast.makeText(context, "gagal load equipment :(", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void initComponents(View view) {
        listOutdoor = view.findViewById(R.id.recyclerEquipOutdoor);
        listBackpack = view.findViewById(R.id.recyclerEquipBackpack);
        listFootwear = view.findViewById(R.id.recyclerEquipFootwear);
    }
}