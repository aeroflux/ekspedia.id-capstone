package ekspedia.id.ekspedia.fragments;


import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.adapter.ListExploreAdapter;
import ekspedia.id.ekspedia.apihelper.BaseApiService;
import ekspedia.id.ekspedia.apihelper.UtilsApi;
import ekspedia.id.ekspedia.model.Gunung;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExploreFragment extends Fragment {

    RecyclerView listGunung;

    ArrayList<Gunung> gunungs;
    ListExploreAdapter adapter;
    String token;

    Context context;

    BaseApiService baseApiService;

    public ExploreFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_explore, container, false);

        context = rootView.getContext();
        Hawk.init(context).build();

        if (Hawk.get("token") != null)
            token = Hawk.get("token").toString();
        baseApiService = UtilsApi.getApiService();

        gunungs = new ArrayList<>();

        listGunung = rootView.findViewById(R.id.recyclerEquipOutdoor);

        if (token != null)
            loadGunung(token);
        adapter = new ListExploreAdapter(context, gunungs);
        listGunung.setAdapter(adapter);
        listGunung.setHasFixedSize(true);
        listGunung.setLayoutManager(new LinearLayoutManager(context));

        // Inflate the layout for this fragment
        return rootView;
    }

    private void loadGunung(String token) {
        final ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setTitle("Loading Data");
        dialog.show();
        if (!gunungs.isEmpty()) {
            adapter.clear();
        }
        baseApiService.getGunung(token)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject result = new JSONObject(response.body().string());
                                if (result.getBoolean("error") == false) {
                                    JSONArray array = result.getJSONObject("data").getJSONArray("gunung");
                                    for (int i = 0; i < array.length(); i++) {
                                        JSONObject object = array.getJSONObject(i);
                                        Gunung gunung = new Gunung(
                                                object.getInt("id"),
                                                object.getInt("popular_count"),
                                                object.getInt("ketinggian"),
                                                object.getInt("bintang_review"),
                                                object.getString("nama"),
                                                object.getString("deskripsi"),
                                                object.getString("latitude"),
                                                object.getString("longitude"),
                                                object.getString("kesulitan"),
                                                object.getJSONArray("file")
                                                        .getJSONObject(0)
                                                        .getString("path")
                                        );
                                        gunungs.add(gunung);
                                    }
                                    adapter.notifyDataSetChanged();
                                } else {
                                    Toast.makeText(context, "gagal load gunung :(", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            } finally {
                                dialog.dismiss();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
    }

}
