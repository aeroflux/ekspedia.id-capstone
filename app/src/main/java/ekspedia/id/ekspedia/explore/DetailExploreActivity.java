package ekspedia.id.ekspedia.explore;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.orhanobut.hawk.Hawk;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ekspedia.id.ekspedia.MapsActivity;
import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.apihelper.BaseApiService;
import ekspedia.id.ekspedia.apihelper.UtilsApi;
import ekspedia.id.ekspedia.model.Gunung;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailExploreActivity extends AppCompatActivity {

    TextView nama, lokasi, harga, deskripsi;
    ImageView image;
    RatingBar rating;
    FloatingActionButton buttonMaps;

    int gunungID;
    String latitude, longitude;

    Context context;
    BaseApiService baseApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_explore);

        context = this;
        baseApiService = UtilsApi.getApiService();

        initComponents();

        if (getIntent() != null) {
            gunungID = getIntent().getIntExtra("id", 0);
            lokasi.setText("Lokasi Gunung");
            harga.setText("Rp 2.500.000");
        }

        if (Hawk.get("token") != null)
            loadInfoGunung();

        buttonMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, MapsActivity.class);
                intent.putExtra("latitude", latitude);
                intent.putExtra("longitude", longitude);
                startActivity(intent);
            }
        });
    }

    private void loadInfoGunung() {
        baseApiService.getGunungDetail(Hawk.get("token").toString(), new Gunung(gunungID))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject result = new JSONObject(response.body().string());
                                if (result.getBoolean("error") == false){
                                    JSONObject object = result.getJSONObject("data").getJSONObject("gunung");
                                    nama.setText(object.getString("nama"));
                                    deskripsi.setText(object.getString("deskripsi"));
                                    rating.setRating(object.getInt("bintang_review"));
                                    latitude = object.getString("latitude");
                                    longitude = object.getString("longitude");

                                    String imageJSON = object.getJSONArray("file")
                                            .getJSONObject(0)
                                            .getString("path");
                                    Glide.with(context).load(imageJSON).into(image);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
    }

    private void initComponents() {
        nama = findViewById(R.id.detailExploreNama);
        lokasi = findViewById(R.id.detailExploreLokasi);
        deskripsi = findViewById(R.id.detailExploreDeskripsi);
        harga = findViewById(R.id.detailExploreHarga);
        image = findViewById(R.id.detailExploreImage);
        rating = findViewById(R.id.detailExploreRating);
        buttonMaps = findViewById(R.id.detailExploreMaps);
    }
}
