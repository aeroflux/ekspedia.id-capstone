package ekspedia.id.ekspedia.equipment;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import ekspedia.id.ekspedia.R;

public class DetailEquipmentActivity extends AppCompatActivity {
    TextView nama, harga, deskripsi;
    RatingBar rating;
    ImageView image;

    int id, kategori_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_equipment);

        initComponents();

        if (getIntent() != null) {
            id = getIntent().getIntExtra("id", 0);
            kategori_id = getIntent().getIntExtra("kategori_id", 0);

            nama.setText(getIntent().getStringExtra("nama"));
            deskripsi.setText(getIntent().getStringExtra("deskripsi"));
            harga.setText(String.valueOf(getIntent().getIntExtra("harga", 0)));
            rating.setRating(getIntent().getIntExtra("rating", 0));

            Glide.with(this).load(getIntent().getStringExtra("foto")).into(image);
        }
    }

    private void initComponents() {
        nama = findViewById(R.id.detailEquipmentNama);
        harga = findViewById(R.id.detailEquipmentHarga);
        deskripsi = findViewById(R.id.detailEquipmentDeskripsi);
        rating = findViewById(R.id.detailEquipmentRating);
        image = findViewById(R.id.detailEquipmentImage);
    }
}