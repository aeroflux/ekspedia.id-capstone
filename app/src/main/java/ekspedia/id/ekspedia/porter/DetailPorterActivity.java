package ekspedia.id.ekspedia.porter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import ekspedia.id.ekspedia.R;

public class DetailPorterActivity extends AppCompatActivity {

    TextView nama, lokasi, desc;
    ImageView photo;
    RatingBar rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_porter);
        getSupportActionBar().setTitle("Profile Porter");

        initComponents();

        if (getIntent() != null)
            nama.setText(getIntent().getStringExtra("nama"));
            lokasi.setText(getIntent().getStringExtra("lokasi"));
            desc.setText(getIntent().getStringExtra("deskripsi"));
            photo.setImageResource(getIntent().getIntExtra("image", 0));
            rating.setRating(getIntent().getIntExtra("rating", 0));
    }

    private void initComponents() {
        nama = findViewById(R.id.detailPorterName);
        lokasi = findViewById(R.id.detailPorterLocation);
        desc = findViewById(R.id.detailPorterDesc);
        photo = findViewById(R.id.detailPorterImage);
        rating = findViewById(R.id.detailPorterRating);
    }
}
