package ekspedia.id.ekspedia.model;

/**
 * Created by Administrator_ on 09/04/2018.
 */

public class Gunung {
    int gunung_id, userID, popular_count, ketinggian;
    int rating;

    String nama, deskripsi, latitude, longitude, kesulitan;
    String photo;

    public Gunung(int gunung_id, int popular_count, int ketinggian, int rating, String nama,
                  String deskripsi, String latitude, String longitude, String kesulitan, String photo) {
        this.gunung_id = gunung_id;
        this.popular_count = popular_count;
        this.ketinggian = ketinggian;
        this.rating = rating;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.latitude = latitude;
        this.longitude = longitude;
        this.kesulitan = kesulitan;
        this.photo = photo;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    // search gunung
    public Gunung(String nama) {
        this.nama = nama;
    }

    public Gunung(int id) {
        this.gunung_id = id;
    }

    // review gunung
    public Gunung(int gunung_id, int userID, int rating, String gunung) {
        this.gunung_id = gunung_id;
        this.userID = userID;
        this.rating = rating;
        this.nama = gunung;
    }

    public int getGunung_id() {
        return gunung_id;
    }

    public void setGunung_id(int gunung_id) {
        this.gunung_id = gunung_id;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getPopular_count() {
        return popular_count;
    }

    public void setPopular_count(int popular_count) {
        this.popular_count = popular_count;
    }

    public int getKetinggian() {
        return ketinggian;
    }

    public void setKetinggian(int ketinggian) {
        this.ketinggian = ketinggian;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getKesulitan() {
        return kesulitan;
    }

    public void setKesulitan(String kesulitan) {
        this.kesulitan = kesulitan;
    }
}
