package ekspedia.id.ekspedia.model;

/**
 * Created by Administrator_ on 10/04/2018.
 */

public class Barang {

    int id, kategori_id, rating, harga;
    String nama, deskripsi, image;

    public Barang(int id, int kategori_id, int rating, int harga, String nama, String deskripsi, String image) {
        this.id = id;
        this.kategori_id = kategori_id;
        this.rating = rating;
        this.harga = harga;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.image = image;
    }

    public Barang(String image, String nama) {
        this.image = image;
        this.nama = nama;
    }

    public Barang(String nama) {
        this.nama = nama;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKategori_id() {
        return kategori_id;
    }

    public void setKategori_id(int kategori_id) {
        this.kategori_id = kategori_id;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}