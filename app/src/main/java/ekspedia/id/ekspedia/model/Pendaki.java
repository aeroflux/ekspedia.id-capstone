package ekspedia.id.ekspedia.model;

import java.io.File;

/**
 * Created by Administrator_ on 04/04/2018.
 */

public class Pendaki {
    String nama, telepon, username, email, password, alamat;

    File file;

    // constructor for register
    public Pendaki(String email, String username, String password, String nama, String phone) {
        this.email = email;
        this.username = username;
        this.password = password;
        this.nama = nama;
        this.telepon = phone;
    }

    // constructor for login
    public Pendaki(String username, String password) {
        this.username = username;
        this.password = password;
    }

    // constructor for profile
    public Pendaki(String nama, String telepon, String alamat) {
        this.nama = nama;
        this.telepon = telepon;
        this.alamat = alamat;
    }

    public Pendaki(String nama, String telepon, String alamat, File photo_file) {
        this.nama = nama;
        this.telepon = telepon;
        this.alamat = alamat;
        this.file = photo_file;
    }

}
