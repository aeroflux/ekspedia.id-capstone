package ekspedia.id.ekspedia.model;

/**
 * Created by Administrator_ on 10/04/2018.
 */

public class Event {
    int id;
    String nama;
    String deskripsi;
    String tanggal;

    public Event(int id, String nama, String deskripsi, String tanggal) {
        this.id = id;
        this.nama = nama;
        this.deskripsi = deskripsi;
        this.tanggal = tanggal;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }
}
