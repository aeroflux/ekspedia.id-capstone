package ekspedia.id.ekspedia.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.orhanobut.hawk.Hawk;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;

import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.apihelper.BaseApiService;
import ekspedia.id.ekspedia.apihelper.UtilsApi;
import ekspedia.id.ekspedia.model.Pendaki;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    EditText editFullname, editEmail, editUsername, editPhone;
    ImageView editPhoto;
    Button buttonEdit;

    ProgressDialog progressDialog;

    String token;

    Context context;
    BaseApiService baseApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profil);

        context = this;
        Hawk.init(context).build();
        baseApiService = UtilsApi.getApiService();

        editPhoto = findViewById(R.id.editPhoto);
        editFullname = findViewById(R.id.editFullname);
        editEmail = findViewById(R.id.editEmail);
        editPhone = findViewById(R.id.editPhone);
        buttonEdit = findViewById(R.id.buttonEdit);

        if (Hawk.get("token") != null) {
            token = Hawk.get("token");
        }

        loadProfile(token);

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editProfile(token);
            }
        });
    }

    private void loadProfile(String token) {
        progressDialog = new ProgressDialog(EditProfileActivity.this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("please wait");
        progressDialog.show();
        baseApiService.getProfile(token).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    try {
                        JSONObject result = new JSONObject(response.body().string());
                        if (result.getBoolean("error") == false) {
                            editFullname.setText(result.getJSONObject("data").getString("nama"));
                            editPhone.setText(result.getJSONObject("data").getString("telepon"));
                            editEmail.setText(result.getJSONObject("data").getString("email"));
                        } else {
                            Toast.makeText(context, "gagal ambil data profil :(", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void editProfile(String token) {
        String fullname = editFullname.getText().toString();
        String phone = editPhone.getText().toString();
        String alamat = "Bandung";

        baseApiService.editProfile(token, new Pendaki(fullname, phone, alamat))
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            try {
                                JSONObject result = new JSONObject(response.body().string());
                                if (result.getBoolean("error") == false) {
                                    Intent intent = new Intent(context, ProfileActivity.class);
                                    startActivity(intent);
                                    finish();
                                } else {
                                    Toast.makeText(context, "gagal edit profile :(", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                    }
                });
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
