package ekspedia.id.ekspedia.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ekspedia.id.ekspedia.MainActivity;
import ekspedia.id.ekspedia.R;
import ekspedia.id.ekspedia.apihelper.BaseApiService;
import ekspedia.id.ekspedia.apihelper.UtilsApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    TextView profileName, profilePhone, profileEmail, profileUsername;
    ImageView profilePhoto;
    Button buttonEdit;

    ProgressDialog progressDialog;

    String token;

    Context context;
    BaseApiService baseApiService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);

        context = this;
        Hawk.init(context).build();
        baseApiService = UtilsApi.getApiService();

        profileUsername = findViewById(R.id.profileUsername);
        profileName = findViewById(R.id.profileName);
        profilePhone = findViewById(R.id.profilePhone);
        profileEmail = findViewById(R.id.profileEmail);
        profilePhoto = findViewById(R.id.profilePhoto);
        buttonEdit = findViewById(R.id.profileEdit);

        if (Hawk.get("token") != null) {
            token = Hawk.get("token").toString();
        }
        loadProfile(token);

        buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, EditProfileActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.opt_profile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.buttonLogout:
                logoutProcess(token);
                Intent profile = new Intent(context, ProfileActivity.class);
                startActivity(profile);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void loadProfile(String token) {
        progressDialog = new ProgressDialog(ProfileActivity.this);
        progressDialog.setTitle("Loading");
        progressDialog.setMessage("please wait");
        progressDialog.show();
        baseApiService.getProfile(token).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    try {
                        JSONObject result = new JSONObject(response.body().string());
                        if (result.getBoolean("error") == false) {
                            profileUsername.setText(result.getJSONObject("data").getString("username"));
                            profileName.setText(result.getJSONObject("data").getString("nama"));
                            profilePhone.setText(result.getJSONObject("data").getString("telepon"));
                            profileEmail.setText(result.getJSONObject("data").getString("email"));
                        } else {
                            Toast.makeText(context, "gagal ambil data profil :(", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void logoutProcess(String token) {
        baseApiService.logout(token).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        JSONObject result = new JSONObject(response.body().string());
                        if (result.getBoolean("error") == false) {
                            Hawk.deleteAll();
                            Intent intent = new Intent(context, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(context, "gagal logout :(", Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }
}
