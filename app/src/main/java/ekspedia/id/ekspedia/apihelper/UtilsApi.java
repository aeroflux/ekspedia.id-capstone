package ekspedia.id.ekspedia.apihelper;

/**
 * Created by Yuda on 2/13/2018.
 */

public class UtilsApi {

    // base url ke localhost
    public static final String BASE_URL_API = "http://api.ekspedia.healtharoundu.com/api/";

    // deklarasi interface BaseApiService
    public static BaseApiService getApiService() {
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }
}
