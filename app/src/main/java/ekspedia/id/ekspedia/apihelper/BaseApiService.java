package ekspedia.id.ekspedia.apihelper;

import ekspedia.id.ekspedia.model.Barang;
import ekspedia.id.ekspedia.model.Brand;
import ekspedia.id.ekspedia.model.Event;
import ekspedia.id.ekspedia.model.Gunung;
import ekspedia.id.ekspedia.model.Lokasi;
import ekspedia.id.ekspedia.model.Pendaki;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Yuda on 2/13/2018.
 */

public interface BaseApiService {

    // authentication
    @Headers("Content-Type: application/json")
    @GET("authentication/pendaki/logout")
    Call<ResponseBody> logout(@Header("X-Ekspedia-Token") String token);

    @Headers("Content-Type: application/json")
    @POST("authentication/pendaki/register")
    Call<ResponseBody> register(@Body Pendaki pendaki);

    @Headers("Content-Type: application/json")
    @POST("authentication/pendaki/login")
    Call<ResponseBody> login(@Body Pendaki pendaki);

    // profile
    @Headers("Content-Type: application/json")
    @GET("pendaki/profile")
    Call<ResponseBody> getProfile(@Header("X-Ekspedia-Token") String token);

    @Headers("Content-Type: application/json")
    @POST("pendaki/profile/edit")
    Call<ResponseBody> editProfile(@Header("X-Ekspedia-Token") String token,
                                   @Body Pendaki pendaki);

    @Multipart
    @POST("pendaki/profile/foto/edit")
    Call<ResponseBody> editFotoProfile(@Header("X-Ekspedia-Token") String token,
                                       @Part("nama") RequestBody nama,
                                       @Part("telepon") RequestBody telepon,
                                       @Part("alamat") RequestBody alamat,
                                       @Part MultipartBody.Part file);

    // explore gunung
    @Headers("Content-Type: application/json")
    @GET("pendaki/gunung/semua")
    Call<ResponseBody> getGunung(@Header("X-Ekspedia-Token") String token);

    @Headers("Content-Type: application/json")
    @POST("pendaki/gunung")
    Call<ResponseBody> getGunungDetail(@Header("X-Ekspedia-Token") String token,
                                       @Body Gunung gunung);

    @Headers("Content-Type: application/json")
    @GET("pendaki/gunung/popular")
    Call<ResponseBody> getPopularGunung(@Header("X-Ekspedia-Token") String token);

    @Headers("Content-Type: application/json")
    @POST("pendaki/gunung/cari")
    Call<ResponseBody> searchGunung(@Header("X-Ekspedia-Token") String token,
                                    @Body Gunung gunung);

    // explore DetailEventActivity
    @Headers("Content-Type: application/json")
    @GET("event/semua")
    Call<ResponseBody> getEvent(@Header("X-Ekspedia-Token") String token);

    @Headers("Content-Type: application/json")
    @POST("event")
    Call<ResponseBody> getEventDetail(@Header("X-Ekspedia-Token") String token,
                                      @Body Event event);

    // preparation
    @Headers("Content-Type: application/json")
    @POST("pendaki/preparation")
    Call<ResponseBody> preparation(@Header("X-Ekspedia-Token") String token,
                                   @Field("gunung_id") int gunung_id,
                                   @Field("jumlah_pendaki") int jumlah);

    // equipment
    @Headers("Content-Type: application/json")
    @GET("brand/semua")
    Call<ResponseBody> getBrand(@Header("X-Ekspedia-Token") String token);

    @Headers("Content-Type: application/json")
    @POST("brand/cari")
    Call<ResponseBody> filterBrand(@Header("X-Ekspedia-Token") String token,
                                   @Body Brand brand);

    @Headers("Content-Type: application/json")
    @POST("filter/barang")
    Call<ResponseBody> filterSearchBarang(@Header("X-Ekspedia-Token") String token,
                                          @Body Barang barang);

    @Headers("Content-Type: application/json")
    @GET("lokasi/semua")
    Call<ResponseBody> getLokasi(@Header("X-Ekspedia-Token") String token);

    @Headers("Content-Type: application/json")
    @POST("lokasi/cari")
    Call<ResponseBody> filterLokasi(@Header("X-Ekspedia-Token") String token,
                                    @Body Lokasi lokasi);

    @Headers("Content-Type: application/json")
    @GET("barang/semua")
    Call<ResponseBody> getAllBarang(@Header("X-Ekspedia-Token") String token);

    @Headers("Content-Type: application/json")
    @POST("barang/kategori")
    Call<ResponseBody> getBarangPerKategori(@Header("X-Ekspedia-Token") String token,
                                            @Body Barang barang);

}
